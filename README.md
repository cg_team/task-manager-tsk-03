# TASK MANAGER

Console application for task list.

# DEVELOPER INFO

NAME: Vsevolod Inshakov

E-MAIL: vinshakov@tsconsulting.com

# SOFTWARE

* JDK 1.8

* Windows OS

# HARDWARE

* RAM 16Gb

* CPU i5

* HDD 128Gb

# RUN PROGRAM

```
java -jar ./task-manager.jar
```

# SCREENSHOTS
https://yadi.sk/d/0JDK7HvcNZHYCw?w=1
